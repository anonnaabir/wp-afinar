=== WP Afinar ===
Tags: gutenberg, agency, business, multi-purpose
Requires at least: 5.0
Tested up to: 6.2
Requires PHP: 7.4
Stable tag: 2.3.2
License: GNU General Public License v2 or later

Afinar is a Gutenberg based WordPress Theme for Multi-Purpose Website.

== Description ==
Afinar is a Gutenberg based WordPress Theme for Multi-Purpose Website. 100% responsive and mobile friendly. Easy to use and customize using Gutenberg Blocks. Compatible with Gutenberg FSE (Full Site Editing) and WooCommerce. Also, comes with built-in Preloader & White Label Branding.

== Changelog ==

2.3.2:
- Added Travel Agency Demo
- Added Car Rental Demo
- Added Tailoring Service Demo
- Added Virtual Reality Demo
- Added Doctor Medical Demo
- Added Web Application Demo
- Added Civil Engineering Demo
- Added Web Design Company Demo
- Added Financial Planning Demo
- Added Business Consulting Demo
- Added Data Science Demo
- Added Business Development Demo

2.3.1:
- Footer Bug Fixed
- JS Console Errors Hidden

2.3:
- Readme Added

2.2:
- Version Number Updated
- Options Updated

2.1:
- Version Number Updated
- Freemius Added

2.0:
- Initial Release
- 16 Demo Added
- Version Number Updated

1.9.9:
- Removed Contacts Custom Post Types
- Version Number Updated

1.9.8:
- Demo Links Updated
- Additional Bug Fixed
- Version Number Updated

1.9.7:
- Added Contact Form
- Added Single Page & Post Template
- Added 404 Page Template
- Added Tracking Code Feature
- Added Custom Header & Footer Code Injector
- Wizard Updated
- Theme Settings Updated

1.9.1:
- Added Demo Importer
- Features Updated
- Bug Fixed
- Version Number Updated

1.9:
- Added Demo Import And Exporter (Beta)
- Version Number Updated

1.8:
- URL Issue Fixed
- Version Number Updated

1.7:
- Responsive Issue Fixed
- Mobile Header Updated
- Bug Fixed
- Version Number Updated

1.6:
- Header & Footer Updated
- Header & Footer Option Panel Updated
- Version Number Updated

1.5:
- Header Template Updated
- Version Number Updated

1.4:
- Page Template Added and Updated
- Bug Fixed

1.3:
- Footer Template Added
- Version Number Updated
- Demo Added

1.2:
- Padding Issue Fixed
- Version Number Updated

1.0:
- First Release