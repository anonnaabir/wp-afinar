<?php

    define('THEME_TEXTDOMAIN', 'afinar-pro');


    if ( ! function_exists( 'wp_afinar_freemius' ) ) {
        // Create a helper function for easy SDK access.
        function wp_afinar_freemius() {
            global $wp_afinar_freemius;
    
            if ( ! isset( $wp_afinar_freemius ) ) {
                // Include Freemius SDK.
                require_once dirname(__FILE__) . '/freemius/start.php';
    
                $wp_afinar_freemius = fs_dynamic_init( array(
                    'id'                  => '12301',
                    'slug'                => 'afinar-pro',
                    'premium_slug'        => 'afinar-pro',
                    'type'                => 'theme',
                    'public_key'          => 'pk_8a1a68e729b39d5d48103e68f0b65',
                    'is_premium'          => true,
                    'is_premium_only'     => true,
                    'has_addons'          => false,
                    'has_paid_plans'      => true,
                    'menu'                => array(
                        'slug'           => 'afinar_options',
                        'contact'        => false,
                        'support'        => false,
                    ),
                    // Set the SDK to work in a sandbox mode (for development & testing).
                    // IMPORTANT: MAKE SURE TO REMOVE SECRET KEY BEFORE DEPLOYMENT.
                    'secret_key'          => 'sk_ee01ytfQ5.CI~QTzLaOx(&7UoXX7w',
                ) );
            }
    
            return $wp_afinar_freemius;
        }
    
        // Init Freemius.
        wp_afinar_freemius();
        // Signal that SDK was initiated.
        do_action( 'wp_afinar_freemius_loaded' );
    }

    require_once('inc/init.php');
    require_once('admin/demo-import.php');
    require_once('admin/admin.php');
    require_once('admin/customizer.php');
    require_once('admin/white-label.php');
    require_once('admin/preloader.php');
    require_once('admin/contact-form.php');
    require_once('admin/tracking-code.php');