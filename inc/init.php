<?php

if (! class_exists ('Afinar_Init') ) {

    class Afinar_Init {
        // create static instance

        private static $instance = false;

        public static function get_instance() {
            if ( !self::$instance )
                self::$instance = new self;
            return self::$instance;
        }

        // constructor
        public function __construct() {
            add_action('after_setup_theme', array( $this,'wp_afinar_theme_setup'));
            add_action('init', array( $this,'register_my_menus'));
            add_action('wp_enqueue_scripts', array( $this,'wp_afinar_scripts'));
            add_filter( 'script_loader_tag', array( $this,'add_module_attribute'), 10,3 );
            add_action( 'customize_register', array($this,'afinar_customizer'));
        }

        public function wp_afinar_theme_setup() {
            // Add support for block styles.
            add_theme_support( 'wp-block-styles' );
        
            // Enqueue editor styles.
            add_editor_style( 'style.css' );

            /** tag-title **/
            add_theme_support( 'title-tag' );

            /** post thumbnail **/
            add_theme_support( 'post-thumbnails' );

            /** HTML5 support **/
            add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );

            add_theme_support( 'align-wide' );

            add_theme_support( 'html5', array( 'navigation-widgets' ) );

            add_theme_support( 'custom-logo', array(
                'height' => 480,
                'width'  => 720,
            ) );

            add_theme_support( 'block-templates' );
            add_theme_support( 'block-template-parts' );

            add_option( 'afinar_customizer', '', '', 'yes' );
            add_option( 'afinar_tracking', '', '', 'yes' );
            add_option( 'afinar_custom_code', '', '', 'yes' );
        }
    
    
        public function wp_afinar_scripts() {
            $afinar_customizer = get_option( 'afinar_customizer' ); 
            $afinar_preloader = get_option( 'afinar_preloader_settings' );
            
            $custom_logo_id = get_theme_mod( 'custom_logo' );
            $logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );

            $header_menu = get_term(get_nav_menu_locations()['afinar-main'], 'nav_menu')->name ?? '';
            $footer_menu = get_term(get_nav_menu_locations()['afinar-footer'], 'nav_menu')->name ?? '';
            $social_menu = get_term(get_nav_menu_locations()['afinar-social'], 'nav_menu')->name ?? '';

            // wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css' );
            wp_enqueue_style( 'app', get_template_directory_uri() . '/assets/app.css' );
            wp_enqueue_style( 'admin', get_template_directory_uri() . '/assets/plugin-vue2_normalizer.css' );
            wp_enqueue_script( 'main', get_template_directory_uri() . '/assets/main.js', [], '1.9.7', false );
            wp_localize_script( 'main', 'afinar_settings', array(
                'customizer' => $afinar_customizer,
                'preloader' => $afinar_preloader,
                'logo' => $logo[0] ?? '',
                'header_menu' => wp_get_nav_menu_items($header_menu) ?? '',
                'footer_menu_name' => $footer_menu ?? '',
                'footer_menu' => wp_get_nav_menu_items($footer_menu) ?? '',
                'social_menu_name' => $social_menu ?? '',
                'social_menu' => wp_get_nav_menu_items($social_menu) ?? '',
                'page_template' => get_page_template_slug(),
                'page_title' => get_the_title(),
                'page_id' => get_the_ID(),
                'page_url' => get_permalink(),
                'page_content' => get_the_content(),
                'page_excerpt' => get_the_excerpt(),
                'page_date' => get_the_date(),
                'page_author' => get_the_author(),
                'page_author_url' => get_author_posts_url( get_the_author_meta( 'ID' ) ),
                'posts' => get_posts(),
            ) );
        }
    
        public function add_module_attribute($tag, $handle, $src) {
            // if not your script, do nothing and return original $tag
            if ( 'main' !== $handle ) {
                return $tag;
            }
            // change the script tag by adding type="module" and return it.
            $tag = '<script type="module" src="' . esc_url( $src ) . '"></script>';
            return $tag;
        }

        public function register_my_menus() {
            register_nav_menus(
              array(
                'afinar-main' => __( 'Afinar Main Menu' ),
                'afinar-footer' => __( 'Afinar Footer Menu' ),
                'afinar-social' => __( 'Afinar Social Menu' )
               )
             );
        }

        public function afinar_customizer($wp_customize) {
            //All our sections, settings, and controls will be added here
         }

    }

}

    // initiate instance
    Afinar_Init::get_instance();