<?php

if (! class_exists ('Afinar_Customizer') ) {

    Class Afinar_Customizer {

        private static $instance = false;

        public static function get_instance() {
            if ( !self::$instance )
                self::$instance = new self;
            return self::$instance;
        }

        public function __construct(){
            add_action( 'rest_api_init', array( $this, 'afinar_customizer_post' ));
        }

        public function afinar_customizer_post(){
            register_rest_route( 'afinar/v1', '/customizer', array(
                'methods' => 'POST',
                'callback' => array( $this, 'afinar_customizer_post_callback' ),
            ));
        }

        public function afinar_customizer_post_callback(WP_REST_Request $request){
            $value = json_decode($request->get_body());
            update_option( 'afinar_customizer', $value );
            return $value;
        }
 
    }

    // initiate instance
    Afinar_Customizer::get_instance();
    
}