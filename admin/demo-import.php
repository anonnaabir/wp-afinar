<?php

if (! class_exists ('Afinar_Demo_Import') ) {

    Class Afinar_Demo_Import {

        private static $instance = false;

        public static function get_instance() {
            if ( !self::$instance )
                self::$instance = new self;
            return self::$instance;
        }

        public function __construct(){
            $lp_options = get_option( 'afinar_options' );
            add_action( 'rest_api_init', array( $this, 'afinar_import_demos' ));
        }

        public function afinar_import_demos(){
            register_rest_route( 'afinar/v1', '/pages/insert', array(
                'methods' => 'POST',
                'callback' => array( $this, 'afinar_pages_post_callback' ),
            ));

            register_rest_route( 'afinar/v1', '/settings/insert', array(
                'methods' => 'POST',
                'callback' => array( $this, 'afinar_settings_post_callback' ),
            ));

            register_rest_route( 'afinar/v1', '/menus/insert', array(
                'methods' => 'POST',
                'callback' => array( $this, 'afinar_menus_post_callback' ),
            ));

            register_rest_route( 'afinar/v1', '/options/insert', array(
                'methods' => 'POST',
                'callback' => array( $this, 'afinar_options_post_callback' ),
            ));
        }

        public function afinar_pages_post_callback(WP_REST_Request $request){
            $response = wp_remote_get($request->get_body());
            $body  = wp_remote_retrieve_body($response);
            $value = json_decode($body);

            foreach($value as $page){
                $my_post = array(
                    'post_title'    => wp_strip_all_tags($page->post_title),
                    'post_content'  => $page->post_content,
                    'post_type'     => 'page',
                    'post_status'   => 'publish',
                    'post_author'   => 1,
                );

                $post_id = wp_insert_post($my_post);

            }

            return $value;
        }


        public function afinar_menus_post_callback(WP_REST_Request $request){
            $response = wp_remote_get($request->get_body());
            $body  = wp_remote_retrieve_body($response);
            $value = json_decode($body);

            
            $header_menu = 'Main Menu';
            $menu_exists = wp_get_nav_menu_object( $header_menu );
            if( !$menu_exists){
                $menu_id = wp_create_nav_menu($header_menu);
                foreach($value[0] as $menu_item){
                    wp_update_nav_menu_item($menu_id, 0, array(
                        'menu-item-title' =>  $menu_item->title,
                        'menu-item-object-id' => $menu_item->ID,
                        'menu-item-url' => $menu_item->url, 
                        'menu-item-status' => 'publish'));
                }

                    $locations = get_theme_mod('nav_menu_locations');
                    $locations['afinar-main'] = $menu_id;
                    set_theme_mod( 'nav_menu_locations', $locations );
            }


            $footer_menu = 'Footer Menu';
            $menu_exists = wp_get_nav_menu_object( $footer_menu );
            if( !$menu_exists){
                $menu_id = wp_create_nav_menu($footer_menu);
                foreach($value[1] as $menu_item){
                    wp_update_nav_menu_item($menu_id, 0, array(
                        'menu-item-title' =>  $menu_item->title,
                        'menu-item-object-id' => $menu_item->ID,
                        'menu-item-url' => $menu_item->url, 
                        'menu-item-status' => 'publish'));
                }

                    $locations = get_theme_mod('nav_menu_locations');
                    $locations['afinar-footer'] = $menu_id;
                    set_theme_mod( 'nav_menu_locations', $locations );
            }

            $social_menu = 'Social Links';
            $menu_exists = wp_get_nav_menu_object( $social_menu );
            if( !$menu_exists){
                $menu_id = wp_create_nav_menu($social_menu);
                foreach($value[2] as $menu_item){
                    wp_update_nav_menu_item($menu_id, 0, array(
                        'menu-item-title' =>  $menu_item->title,
                        'menu-item-object-id' => $menu_item->ID,
                        'menu-item-url' => $menu_item->url, 
                        'menu-item-status' => 'publish'));
                }

                    $locations = get_theme_mod('nav_menu_locations');
                    $locations['afinar-social'] = $menu_id;
                    set_theme_mod( 'nav_menu_locations', $locations );
            }


            // add th menu to the location
            // $locations = get_theme_mod('nav_menu_locations');
            // $locations['main-menu'] = $menu_id;
            // set_theme_mod( 'nav_menu_locations', $locations );


            return $value;
        }

        public function afinar_settings_post_callback(WP_REST_Request $request){
            $response = wp_remote_get($request->get_body());
            $body  = wp_remote_retrieve_body($response);
            $value = json_decode($body);

            update_option( 'afinar_customizer', $value );
            return $value;
        }

        public function afinar_options_post_callback(WP_REST_Request $request){
            $response = wp_remote_get($request->get_body());
            $body  = wp_remote_retrieve_body($response);
            $value = json_decode($body);

            // foreach ($value as $key => $value) {
            //     update_option( $key, $value );
            // }

            

            // // update_option( 'afinar_customizer', $value );
            return $value;
        }
 
    }

    Afinar_Demo_Import::get_instance();
    
}