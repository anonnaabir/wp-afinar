<?php

if (! class_exists ('Afinar_Tracking_Code') ) {

    Class Afinar_Tracking_Code {

        private static $instance = false;

        public static function get_instance() {
            if ( !self::$instance )
                self::$instance = new self;
            return self::$instance;
        }

        public function __construct(){
            add_action( 'rest_api_init', array( $this, 'afinar_tracking_code' ));
            add_action('wp_head', array($this,'add_tracking_code'));
            add_action('wp_head', array($this,'add_header_code'));
            add_action('wp_footer', array($this,'add_footer_code'));
        }

        public function afinar_tracking_code(){
            register_rest_route( 'afinar/v1', '/code/tracking', array(
                'methods' => 'POST',
                'callback' => array( $this, 'afinar_tracking_post_callback' ),
            ));

            register_rest_route( 'afinar/v1', '/code/inject', array(
                'methods' => 'POST',
                'callback' => array( $this, 'afinar_inject_post_callback' ),
            ));
        }

        public function afinar_tracking_post_callback(WP_REST_Request $request){
            $value = json_decode($request->get_body());
            update_option( 'afinar_tracking', $value );
            return $value;
        }

        public function afinar_inject_post_callback(WP_REST_Request $request){
            $value = json_decode($request->get_body());
            update_option( 'afinar_custom_code', $value );
            return $value;
        }

        public function add_tracking_code(){
            // get the option
            $afinar_tracking = get_option('afinar_tracking');

            // Google Analytics
            if (isset($afinar_tracking->googleanalytics)) {
                
                $google_analytics = <<<EOD
                <!-- Google tag (gtag.js) -->
                <script async src="https://www.googletagmanager.com/gtag/js?id={$afinar_tracking->googleanalytics}"></script>
                <script>
                  window.dataLayer = window.dataLayer || [];
                  function gtag(){window.dataLayer.push(arguments);}
                  gtag('js', new Date());
                
                  gtag('config', '{$afinar_tracking->googleanalytics}');
                </script>
                EOD;

                echo $google_analytics;
            }

            // Google Search Console

            if (isset($afinar_tracking->googlesearchconsole)) {
                
                $google_search_console = <<<EOD
                <!-- Google Search Console -->
                <meta name="google-site-verification" content="{$afinar_tracking->googlesearchconsole}" />
                EOD;

                echo $google_search_console;
            }

            // Facebook Pixel

            if (isset($afinar_tracking->facebookpixel)) {
                
                $facebook_pixel = <<<EOD
                <!-- Facebook Pixel -->
                <script>
                !function(f,b,e,v,n,t,s)
                {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
                n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t,s)}(window, document,'script',
                'https://connect.facebook.net/en_US/fbevents.js');
                fbq('init', '{$afinar_tracking->facebookpixel}');
                fbq('track', 'PageView');
                </script>
                <noscript><img height="1" width="1" style="display:none"
                src="https://www.facebook.com/tr?id={$afinar_tracking->facebookpixel}&ev=PageView&noscript=1"
                /></noscript>
                EOD;

                echo $facebook_pixel;
            }

            // Hotjar

            if (isset($afinar_tracking->hotjar)) {
                
                $hotjar = <<<EOD
                <!-- Hotjar Tracking Code for https://afinar.com/ -->
                <script>
                (function(h,o,t,j,a,r){
                    h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
                    h._hjSettings={hjid:{$afinar_tracking->hotjar},hjsv:6};
                    a=o.getElementsByTagName('head')[0];
                    r=o.createElement('script');r.async=1;
                    r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
                    a.appendChild(r);
                })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
                </script>
                EOD;

                echo $hotjar;
            }

            // Google Tag Manager

            if (isset($afinar_tracking->googletagmanager)) {
                
                $google_tag_manager = <<<EOD
                <!-- Google Tag Manager -->
                <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                })(window,document,'script','dataLayer','{$afinar_tracking->googletagmanager}');</script>
                <!-- End Google Tag Manager -->
                EOD;

                echo $google_tag_manager;
            }

            // Google Ads

            if (isset($afinar_tracking->googleads)) {
                
                $google_ads = <<<EOD
                <!-- Google Ads -->
                <script async src="https://www.googletagmanager.com/gtag/js?id={$afinar_tracking->googleads}"></script>
                <script>
                  window.dataLayer = window.dataLayer || [];
                  function gtag(){dataLayer.push(arguments);}
                  gtag('js', new Date());
                
                  gtag('config', '{$afinar_tracking->googleads}');
                </script>
                EOD;

                echo $google_ads;
            }


            // Bing Webmaster

            if (isset($afinar_tracking->bingwebmaster)) {
                
                $bing_webmaster = <<<EOD
                <!-- Bing Webmaster -->
                <meta name="msvalidate.01" content="{$afinar_tracking->bingwebmaster}" />
                EOD;

                echo $bing_webmaster;
            }


            // Bing Analytics

            if (isset($afinar_tracking->binganalytics)) {
                
                $bing_analytics = <<<EOD
                <!-- Bing Analytics -->
                <script>(function(w,d,t,r,u){var f,n,i;w[u]=w[u]||[],f=function(){var o={ti:"{$afinar_tracking->binganalytics}"};o.q=w[u],w[u]=new UET(o),w[u].push("pageLoad")},n=d.createElement(t),n.src=r,n.async=1,n.onload=n.onreadystatechange=function(){var s=this.readyState;s&&s!=="loaded"&&s!=="complete"||(f(),n.onload=n.onreadystatechange=null)},i=d.getElementsByTagName(t)[0],i.parentNode.insertBefore(n,i)})(window,document,"script","//bat.bing.com/bat.js","uetq");</script>
                EOD;

                echo $bing_analytics;
            }

            // Yandex Metrica

            if (isset($afinar_tracking->yandexmetrica)) {
                
                $yandex_metrica = <<<EOD
                <!-- Yandex Metrica -->
                <script type="text/javascript">
                (function (d, w, c) {
                    (w[c] = w[c] || []).push(function() {
                        try {
                            w.yaCounter{$afinar_tracking->yandexmetrica} = new Ya.Metrika({
                                id:{$afinar_tracking->yandexmetrica},
                                clickmap:true,
                                trackLinks:true,
                                accurateTrackBounce:true,
                                webvisor:true
                            });
                        } catch(e) { }
                    });
                
                    var n = d.getElementsByTagName("script")[0],
                        s = d.createElement("script"),
                        f = function () { n.parentNode.insertBefore(s, n); };
                    s.type = "text/javascript";
                    s.async = true;
                    s.src = "https://mc.yandex.ru/metrika/watch.js";
                
                    if (w.opera == "[object Opera]") {
                        d.addEventListener("DOMContentLoaded", f, false);
                    } else { f(); }
                })(document, window, "yandex_metrika_callbacks");
                </script>
                <noscript><div><img src="https://mc.yandex.ru/watch/{$afinar_tracking->yandexmetrica}" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
                EOD;

                echo $yandex_metrica;
            }

            // Yandex Webmaster

            if (isset($afinar_tracking->yandexwebmaster)) {
                
                $yandex_webmaster = <<<EOD
                <!-- Yandex Webmaster -->
                <meta name="yandex-verification" content="{$afinar_tracking->yandexwebmaster}" />
                EOD;

                echo $yandex_webmaster;
            }

            // Pinterest

            if (isset($afinar_tracking->pinterest)) {
                
                $pinterest = <<<EOD
                <!-- Pinterest -->
                <script type="text/javascript">
                !function(e){if(!window.pintrk){window.pintrk = function () {
                    window.pintrk.queue.push(Array.prototype.slice.call(arguments));
                };
                var n=window.pintrk;n.queue=[],n.version="3.0";
                var t=document.createElement("script");
                t.async=!0,t.src=e;
                var r=document.getElementsByTagName("script")[0];
                r.parentNode.insertBefore(t,r);
                }}("https://s.pinimg.com/ct/core.js");
                pintrk('load', '{$afinar_tracking->pinterest}', {em: '<user_email_address>'});
                pintrk('page');
                </script>
                <noscript>
                <img height="1" width="1" style="display:none;" alt="" src="https://ct.pinterest.com/v3/?tid={$afinar_tracking->pinterest}&noscript=1" />
                </noscript>
                EOD;

                echo $pinterest;
            }

            // Twitter

            if (isset($afinar_tracking->twitter)) {
                
                $twitter = <<<EOD
                <!-- Twitter -->
                <script async src="https://platform.twitter.com/oct.js" type="text/javascript"></script>
                <script type="text/javascript">twttr.conversion.trackPid('{$afinar_tracking->twitter}', { tw_sale_amount: 0, tw_order_quantity: 0 });</script>
                <noscript>
                <img height="1" width="1" style="display:none;" alt="" src="https://analytics.twitter.com/i/adsct?txn_id={$afinar_tracking->twitter}&p_id=Twitter" />
                <img height="1" width="1" style="display:none;" alt="" src="//t.co/i/adsct?txn_id={$afinar_tracking->twitter}&p_id=Twitter" />
                </noscript>
                EOD;

                echo $twitter;
            }


            
        }

        public function add_header_code(){
            // get options
            $afinar_custom_code = get_option('afinar_custom_code');

            if(isset($afinar_custom_code->headercode)){
                echo $afinar_custom_code->headercode;
            }
        }

        public function add_footer_code(){
            // get options
            $afinar_custom_code = get_option('afinar_custom_code');

            if(isset($afinar_custom_code->footercode)){
                echo $afinar_custom_code->footercode;
            }
        }
 
    }

    // initiate instance
    Afinar_Tracking_Code::get_instance();
    
}