<?php

    class Afinar_Admin_Menu {
        // create static instance

        private static $instance = false;

        public static function get_instance() {
            if ( !self::$instance )
                self::$instance = new self;
            return self::$instance;
        }

        // constructor
        public function __construct() {
            add_action('admin_menu', array( $this,'wp_afiner_admin_menu'));
            add_action('admin_enqueue_scripts', array( $this,'wp_afiner_admin_scripts'));
            add_filter( 'script_loader_tag', array( $this,'add_module_attribute'), 10,3 );
        }


        public function wp_afiner_admin_scripts() {
            $current_screen = get_current_screen();
            $screen_1 = 'toplevel_page_afinar_options';
            $screen_2 = 'afinar-settings_page_afinar_wizard';
            $screen_3 = 'afinar-settings_page_afinar_white_label';
            $screen_4 = 'afinar-settings_page_afinar_required_plugins';

            $customizer_settings = get_option('afinar_customizer');
            $preloader_settings = get_option('afinar_preloader_settings');
            $whitelabel_settings = get_option('afinar_white_label_settings');
            $tracking_settings = get_option('afinar_tracking');
            $custom_code_settings = get_option('afinar_custom_code');
            
            $all_pages = get_pages();
            // get all menus and their items
            $header_menu = get_term(get_nav_menu_locations()['afinar-main'], 'nav_menu')->name ?? '';
            $footer_menu = get_term(get_nav_menu_locations()['afinar-footer'], 'nav_menu')->name ?? '';
            $social_menu = get_term(get_nav_menu_locations()['afinar-social'], 'nav_menu')->name ?? '';
            
            $all_options = wp_load_alloptions();
            $my_options  = array();


            if ($screen_1 == $current_screen->base OR $screen_2 == $current_screen->base OR $screen_3 == $current_screen->base OR $screen_4 == $current_screen->base) {
                wp_enqueue_style( 'app', get_template_directory_uri() . '/assets/app.css' );
                wp_enqueue_style( 'admin', get_template_directory_uri() . '/assets/plugin-vue2_normalizer.css' );
                wp_enqueue_script( 'admin', get_template_directory_uri() . '/assets/admin.js', [], '1.9.7', true );
                wp_localize_script( 'admin', 'api_settings', array(
                    'root' => esc_url_raw( rest_url() ),
                    'nonce' => wp_create_nonce('wp_rest'),
                    'customizer' => $customizer_settings,
                    'preloader' => $preloader_settings,
                    'whitelabel' => $whitelabel_settings,
                    'tracking' => $tracking_settings,
                    'custom_code' => $custom_code_settings,
                    'logo' => get_template_directory_uri() . '/inc/logo.png',
                    'all_pages' => $all_pages,
                    'header_menu' => wp_get_nav_menu_items($header_menu) ?? '',
                    'footer_menu' => wp_get_nav_menu_items($footer_menu) ?? '',
                    'social_menu' => wp_get_nav_menu_items($social_menu) ?? '',
                    'all_options' => $all_options,
                ) );
            }
        
    }


    public function add_module_attribute($tag, $handle, $src) {
        // if not your script, do nothing and return original $tag
        if ( 'admin' !== $handle ) {
            return $tag;
        }
        // change the script tag by adding type="module" and return it.
        $tag = '<script type="module" src="' . esc_url( $src ) . '"></script>';
        return $tag;
    }

        public function wp_afiner_admin_menu() {
            $page_title = 'Afinar Settings';
            $menu_title = 'Afinar Settings';
            $capability = 'manage_options';
            $slug = 'afinar_options';
            $icon_url =  get_template_directory_uri() . '/inc/icon.svg';
            
            add_menu_page(__( $page_title, THEME_TEXTDOMAIN ),$menu_title,$capability,$slug,array( $this,'afinar_settings_contents'),$icon_url, 99);

            add_submenu_page(
                $slug,
                __( 'Afinar Wizard', THEME_TEXTDOMAIN),
                __( 'Afinar Wizard', THEME_TEXTDOMAIN),
                'manage_options',
                'afinar_options#/wizard',
                array( $this,'afinar_settings_contents'),
            );

            add_submenu_page(
                $slug,
                __( 'Ready Made Sites', THEME_TEXTDOMAIN),
                __( 'Ready Made Sites', THEME_TEXTDOMAIN),
                'manage_options',
                'afinar_options#/demos',
                array( $this,'afinar_settings_contents'),
            );

            add_submenu_page(
                $slug,
                __( 'Theme Settings', THEME_TEXTDOMAIN),
                __( 'Theme Settings', THEME_TEXTDOMAIN),
                'manage_options',
                'afinar_options#/customizer',
                array( $this,'afinar_settings_contents'),
            );

            add_submenu_page(
                $slug,
                __( 'Preloader Settings', THEME_TEXTDOMAIN),
                __( 'Preloader Settings', THEME_TEXTDOMAIN),
                'manage_options',
                'afinar_options#/preloader',
                array( $this,'afinar_settings_contents'),
            );

            add_submenu_page(
                $slug,
                __( 'White Label Settings', THEME_TEXTDOMAIN),
                __( 'White Label Settings', THEME_TEXTDOMAIN),
                'manage_options',
                'afinar_options#/whitelabel',
                array( $this,'afinar_settings_contents'),
            );

            add_submenu_page(
                $slug,
                __( 'Tracking Code', THEME_TEXTDOMAIN),
                __( 'Tracking Code', THEME_TEXTDOMAIN),
                'manage_options',
                'afinar_options#/tracking-code',
                array( $this,'afinar_settings_contents'),
            );

            add_submenu_page(
                $slug,
                __( 'Code Injector', THEME_TEXTDOMAIN),
                __( 'Code Injector', THEME_TEXTDOMAIN),
                'manage_options',
                'afinar_options#/code-injector',
                array( $this,'afinar_settings_contents'),
            );

            add_submenu_page(
                $slug,
                __( 'Import / Export Settings', THEME_TEXTDOMAIN),
                __( 'Import / Export', THEME_TEXTDOMAIN),
                'manage_options',
                'afinar_options#/import-export',
                array( $this,'afinar_settings_contents'),
            );

            // add_submenu_page(
            //     $slug,
            //     __( 'Afinar Licence Details', THEME_TEXTDOMAIN),
            //     __( 'Licence Details', THEME_TEXTDOMAIN),
            //     'manage_options',
            //     'afinar_license_details',
            //     array( $this,'afinar_white_label'),
            // );

            // Remove Parent Slug
            remove_submenu_page('afinar_options','afinar_options'); 

        }
    
        public function afinar_settings_contents() {
            ?>
            <div id="afinar-admin"></div>
            <?php
        }

        public function afinar_wizard() {
            ?>
            <div id="afinar-wizard"></div>
            <?php
        }

        public function afinar_required_plugins() {
            ?>
            <div id="app"></div>
            <?php
        }
    
        public function afinar_white_label() {
            ?>
            <div id="white-label"></div>
            <?php
        }

    }

    // initiate instance
    Afinar_Admin_Menu::get_instance();

    

    // function export_json(){
    //     $args = array(
    //         'post_type' => 'page',
    //         'page_id' => 62,
    //     );

    //     $the_query = new WP_Query( $args );
 
    //     if ( $the_query->have_posts() ) {
    //         while ( $the_query->have_posts() ) {
    //             $the_query->the_post();
    //             $page_content = get_the_content();
    //             // encode array to json
    //             $json = json_encode($page_content);
    //             file_put_contents("data.json", $json);
    //         }
    //     } else {

    //     wp_reset_postdata();
    // }
    
    // }
    // add_action('init', 'export_json');

    // function import_post(){
    //     $json = file_get_contents("data.json");
    //     $data = json_decode($json, true);
    //     $my_post = array(
    //         'post_title'    => 'Test Post',
    //         'post_content'  => $data,
    //         'post_status'   => 'publish',
    //       );
           
    //       // Insert the post into the database
    //       wp_insert_post( $my_post );
    // }
    
    // add_action('init','import_post');