<?php

if (! class_exists ('Afinar_Contact') ) {

    Class Afinar_Contact {

        private static $instance = false;

        public static function get_instance() {
            if ( !self::$instance )
                self::$instance = new self;
            return self::$instance;
        }

        public function __construct(){
            add_action( 'rest_api_init', array( $this, 'afinar_contact_post' ));
        }

        public function afinar_contact_post(){
            register_rest_route( 'afinar/v1', '/contact/send', array(
                'methods' => 'POST',
                'callback' => array( $this, 'afinar_contact_post_callback' ),
            ));
        }

        public function afinar_contact_post_callback(WP_REST_Request $request){
            $value = json_decode($request->get_body());
            // wordpress mail function
            $to = get_option( 'admin_email' );
            $subject = $value->subject;
            $message = '  Email: ' . $value->email . '  Message: ' . $value->message;
            $headers = array('Content-Type: text/html; charset=UTF-8');
            wp_mail( $to, $subject, $message, $headers );

            return $value;
        }
 
    }

    // initiate instance
    Afinar_Contact::get_instance();
    
}