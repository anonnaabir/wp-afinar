<?php

if (! class_exists ('Afinar_Preloader') ) {

    Class Afinar_Preloader {

        private static $instance = false;

        public static function get_instance() {
            if ( !self::$instance )
                self::$instance = new self;
            return self::$instance;
        }

        public function __construct(){
            $afinar_preloader = get_option('afinar_preloader_settings');

            add_action('wp_enqueue_scripts', array( $this,'wp_afinar_preloader_scripts'));
            add_action( 'rest_api_init', array( $this, 'afinar_preloader_post' ));
        }

        public function wp_afinar_preloader_scripts() {
            if (isset($afinar_preloader) && $afinar_preloader->preloader == true) {
            wp_enqueue_style( 'preloader', get_template_directory_uri() . '/assets/plugin-vue2_normalizer.css');
            }
        }

        public function afinar_preloader_post(){
            register_rest_route( 'afinar/v1', '/preloader', array(
                'methods' => 'POST',
                'callback' => array( $this, 'afinar_preloader_post_callback' ),
            ));
        }

        public function afinar_preloader_post_callback(WP_REST_Request $request){
            $value = json_decode($request->get_body());
            update_option( 'afinar_preloader_settings', $value );
            return $value;
        }
 
    }

    // initiate instance
    Afinar_Preloader::get_instance();
    
}