<?php

if (! class_exists ('Afinar_White_Label') ) {

    Class Afinar_White_Label {

        private static $instance = false;

        public static function get_instance() {
            if ( !self::$instance )
                self::$instance = new self;
            return self::$instance;
        }

        public function __construct(){
            $lp_options = get_option( 'afinar_options' );
            $afinar_white_label = get_option('afinar_white_label_settings'); 

            if (isset($afinar_white_label->whitelabel)) {
                if ($afinar_white_label->whitelabel == true) {
                    add_filter( 'wp_prepare_themes_for_js', array( $this, 'afinar_data_update' ));
                }
                else {
                    return false;
                }
            }

            add_action( 'rest_api_init', array( $this, 'afinar_white_label_post' ));
        }

        public function afinar_white_label_post(){
            register_rest_route( 'afinar/v1', '/white-label', array(
                'methods' => 'POST',
                'callback' => array( $this, 'afinar_white_label_rest_callback' ),
            ));
        }

        public function afinar_white_label_rest_callback(WP_REST_Request $request){
            $value = json_decode($request->get_body());
            update_option( 'afinar_white_label_settings', $value );
            return $value;
        }


        public function afinar_data_update($themes) {
            $afinar_options = get_option('afinar_white_label_settings');

            // Theme Name
            $themes['wp-afinar']['name']= $afinar_options->theme_name ?? $themes['wp-afinar']['name'];
            
            // Theme Description
			$themes['wp-afinar']['description']= $afinar_options->theme_description ?? $themes['wp-afinar']['description'];

            // Theme Version 
            $themes['wp-afinar']['version']= $afinar_options->theme_version ?? $themes['wp-afinar']['version'];

            // Theme Author
			$themes['wp-afinar']['author']= $afinar_options->theme_author ?? $themes['wp-afinar']['author'];

            // Theme Author & URL
			$themes['wp-afinar']['authorAndUri']= $afinar_options->theme_authorUri ?? $themes['wp-afinar']['authorAndUri'];

            // Theme Screenshot
			$themes['wp-afinar']['screenshot'][0] = $afinar_options->theme_screenshot ?? '';

            // Theme Tags
			$themes['wp-afinar']['tags']= $afinar_options->theme_tags ?? '';

			return $themes;
        }
        
 
    }

    // $lp_whitelabel_settings = new Afinar_White_Label();
    // initiate instance
    Afinar_White_Label::get_instance();
    
}