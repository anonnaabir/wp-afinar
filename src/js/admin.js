import Vue from 'vue';
import VueRouter from 'vue-router';
import Vuesax from 'vuesax';
import 'vuesax/dist/vuesax.css';

import Index from './Index.vue'
import Wizard from './Wizard.vue';
import Demos from './Demos.vue';
import ThemeCustomizer from './ThemeCustomizer.vue';
import PreloaderSettings from './PreloaderSettings.vue';
import WhiteLabel from './WhiteLabel.vue';
import TrackingCode from './TrackingCode.vue';
import CodeInjector from './CodeInjector.vue';
import ImportExport from './ImportExport.vue';
import ImportSettings from './ImportSettings.vue';

import AdminContact from './AdminContact.vue';

Vue.use(VueRouter)

const routes = [
    { path: '/', component: Wizard },
    { path: '/wizard', component: Wizard },
    { path: '/demos', component: Demos },
    { path: '/customizer', component: ThemeCustomizer },
    { path: '/preloader', component: PreloaderSettings },
    { path: '/whitelabel', component: WhiteLabel },
    { path: '/tracking-code', component: TrackingCode },
    { path: '/code-injector', component: CodeInjector },
    { path: '/import-export', component: ImportExport },
    { path: '/import', component: ImportSettings },
]

const router = new VueRouter({routes})

new Vue({
    el: '#afinar-admin',
    render: h => h(Index),
    router: router
    // components: { App }
});

new Vue({
    el: '#admin-contact',
    render: h => h(AdminContact),
    // components: { App }
});

// new Vue({
//     el: '#afinar-wizard',
//     render: h => h(Wizard)
//     // components: { App }
// });


// new Vue({
//     el: '#white-label',
//     render: h => h(WhiteLabel)
//     // components: { App }
// });