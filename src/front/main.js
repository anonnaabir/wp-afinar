import Vue from 'vue';
import Vuesax from 'vuesax';
import 'vuesax/dist/vuesax.css';

import Header from './Components/Header.vue';
import Footer from './Components/Footer.vue';
import Blog from './Components/Blog.vue';
import SinglePost from './Components/SinglePost.vue';
import Contact from './Components/Contact.vue';
import Preloader from './Preloader.vue';

// console.log(afinar_settings.menu[2].menu_item_parent);

// document.getElementsByClassName('page-template').innerHTML = '<ol><li>html data</li></ol>';

new Vue({
    el: '#wp-afinar',
    render: h => h(Preloader)
    // components: { App }
});

new Vue({
    el: '#afinar-header',
    render: h => h(Header)
    // components: { App }
});

new Vue({
    el: '#afinar-footer',
    render: h => h(Footer)
    // components: { App }
});

new Vue({
    el: '#afinar-archive',
    render: h => h(Blog)
    // components: { App }
});

new Vue({
    el: '#afinar-single',
    render: h => h(SinglePost)
    // components: { App }
});

new Vue({
    el: '#afinar-contact',
    render: h => h(Contact)
    // components: { App }
});